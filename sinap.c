#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

typedef unsigned U;
typedef unsigned char UC;

typedef struct occurrences {
	U ones;
	U twos;
	U threes;
	U fours;
	U fives;
	U sixes;
} Occurrences;

typedef struct player {
	UC id;
	const char *name;
	U score;
} Player;

static UC random_number(const UC from, const UC to)
{
	assert(from <= to);
	return (UC)((rand() % (to - from + 1)) + from);
}

static void roll_dice(UC *dice, const U dice_count)
{
	U i;
	for (i = 0; i < dice_count; i++)
		dice[i] = random_number(1, 6);
}

static void print_dice(const UC *dice, const U dice_count)
{
	U i;
	for (i = 0; i < dice_count; i++)
		printf("%u ", dice[i]);
}

static Occurrences calculate_occurrences(const UC *dice, const U dice_count)
{
	U i;
	Occurrences o = {0, 0, 0, 0, 0, 0};
	assert (dice_count <= 6);
	for (i = 0; i < dice_count; i++) {
		switch (dice[i]) {
			case 1: o.ones++; break;
			case 2: o.twos++; break;
			case 3: o.threes++; break;
			case 4: o.fours++; break;
			case 5: o.fives++; break;
			case 6: o.sixes++; break;
			default:
				fprintf(stderr, "Invalid dice\n");
				abort();
		}
	}
	return o;
}


static U calc_score(const Occurrences o)
{
	U score = 0;
	if (o.ones == 1 && o.twos == 1 && o.threes == 1
			&& o.fours == 1 && o.fives == 1 && o.sixes == 1) {
		score = 120;
	} else {
		if (o.ones >= 3) {
			score += (o.ones - 2) * 100;
		} else if (o.ones > 0) {
			score += o.ones * 10;
		}
		if (o.twos >= 3) {
			score += (o.twos - 2) * 20;
		}
		if (o.threes >= 3) {
			score += (o.threes - 2) * 30;
		}
		if (o.fours >= 3) {
			score += (o.fours - 2) * 40;
		}
		if (o.fives >= 3) {
			score += (o.fives - 2) * 50;
		} else if (o.fives > 0) {
			score += o.fives * 5;
		}
		if (o.sixes >= 3) {
			score += (o.sixes - 2) * 60;
		}
	}
	return score;
}

static U remaining_dice(const Occurrences o, const U dice_count)
{
	U rd = dice_count;
	if (o.ones == 1 && o.twos == 1 && o.threes == 1 &&
			o.fours == 1 && o.fives == 1 && o.sixes == 1) {
		rd = 6U;
	} else {
		rd -= o.ones;
		if (o.twos > 2) rd -= o.twos;
		if (o.threes > 2) rd -= o.threes;
		if (o.fours > 2) rd -= o.fours;
		rd -= o.fives;
		if (o.sixes > 2) rd -= o.sixes;
	}
	return rd == 0 ? 6U : rd;
}

void sinap(void)
{
	U score_limit = 1000U, accG, accL, dice_count, r_dice_count;
	UC dice[6];
	int c;
	Occurrences o;
	Player a = {0, "A", 0}, b = {1, "B", 0},
	       *p = random_number(0,1) == 0 ? &a : &b;

	while (a.score < score_limit && b.score < score_limit) {
		printf("\nScore: '%s'=%u '%s'=%u. Press enter to continue..",
				a.name, a.score, b.name, b.score);
		(void) fflush(stdout);
		while (getchar() != '\n');
		/* switch player */
		p = p->id == a.id ? &b : &a;
		accG = accL = 0;
		dice_count = r_dice_count = 6U;
		while (1) {
			printf("Player '%s' rolls: ", p->name);
			roll_dice(dice, dice_count);
			print_dice(dice, dice_count);
			o = calculate_occurrences(dice, dice_count);
			accL = calc_score(o);
			r_dice_count = remaining_dice(o, dice_count);
			printf(" ## s=%u ", accL);
			if (accL > 0) {
				printf("S=%u ", accL + accG);
				printf("t=%u ", p->score);
				printf("T=%u ", accL + accG + p->score);
				printf("r=%u", r_dice_count);
			}
			putchar('\n');
			if (accL == 0) {
				break;
			} else {
				dice_count = r_dice_count;
				accG += accL;
				if (accG + p->score > score_limit) {
					p->score += accG;
					break;
				}
			}
			if (!(p->score == 0 && accG < 35)) {
				printf("Continue? [Y/n]: ");
				(void) fflush(stdout);
				c = getchar();
				if (c != '\n')
					while (getchar() != '\n');
				if (c == 'n') {
					p->score += accG;
					break;
				}
			}
		}
	}

	printf("Player '%s' wins with score %u.\n", p->name, p->score);
}

int main(int argc, char **argv)
{
	if (argc > 1) {
		fprintf(stderr, "Usage: %s\n", *argv);
		return 1;
	}
	srand((U) time(NULL));
	puts("SINAP IS NOT A PRIME\n");
	sinap();
	return 0;
}
