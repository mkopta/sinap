CC        = gcc
CFLAGS    =  -Wextra -Wconversion -Wwrite-strings
CFLAGS    += -W -Wall -ansi -pedantic -Wcast-align
CFLAGS    += -Wcast-qual -Wchar-subscripts -Winline
CFLAGS    += -Wpointer-arith -Wredundant-decls -Wshadow
PREFIX    = /usr/local/
args      =

all: clean sinap run

sinap: sinap.c
	$(CC) $(CFLAGS) -o sinap sinap.c

run:
	./sinap

clean:
	rm -f sinap

install: sinap
	mkdir -p $(PREFIX)/bin
	cp sinap $(PREFIX)/bin

uninstall:
	rm -f $(PREFIX)/bin/sinap
